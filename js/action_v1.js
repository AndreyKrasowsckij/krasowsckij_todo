// Model
let state_list = [];        //Список с данными задач
let page_size = 6;          //Количество отображаемых записей на странице
let current_page = 0;       //Текущая страницы
let view_status = 0;    //Включён ли фильтр выполненных
let i;                      //Удобности итератора



// Controller
$(document).ready( function () {

    //Инициация стартовых параметров и кэширование
    let adder_button = $(".adder__button");
    let adder_input = $(".adder__input");
    let filter_button_all =  $(".filter__button-all");
    let filter_button_done = $(".filter__button-done");
    let filter_button_not_done = $(".filter__button-not-done");
    let list__ul = $(".list__ul");
    let counter_text_done = $(".counter__text-done");
    let counter_text_not_done = $(".counter__text-not-done");
    let counter_text_all = $(".counter__text-all");
    let pager = $(".pager");
    let deleter_button = $(".deleter__button");
    let changer_button_not_done = $(".changer__button-not-done");
    let changer_button_done = $(".changer__button-done");
    let list__ul_li_func = (value_this) => ( $(`.list__ul_li${value_this} .li__button-change`) );
    start();



    // Событийные функции
    //Добавление новой задачи
    adder_button.click( function () {
        if (adder_input.val().trim() != '') {
            state_list.push({status: false, data: _.escape(_.unescape(adder_input.val().trim())), state: view_status !== 1 });
            if (page_calculator() != 0) { current_page = page_calculator() - 1; }
            clear_input();
            update();
        }
        return false;
    });

    //Добавление новой задачи по нажатию Enter
    $(".page").keypress( function (event) {
        if (event.key === "Enter") {
            if (adder_input.val().trim() != '') {
                if(focus_control())
                {
                    state_list.push({status: false, data: _.escape(_.unescape(adder_input.val().trim())), state: view_status !== 1 });
                    if (page_calculator() != 0) { current_page = page_calculator() - 1; }
                    clear_input();
                    update();
                }
            }
        }
    });

    //Изменение страницы
    pager.on("click", ".pager__button", function () {
        current_page = parseInt($(this).html()) - 1;
        update();
        return false;
    });

    //Фильтрация всех задач
    filter_button_all.click( function () {
        view_status = 0;
        focus_filter(view_status);
        for (i = 0; i < state_list.length; i++) {
            state_list[i].state = true;
        }
        current_page = 0;
        update();
        return false;
    });

    //Фильтрация выполненных задач
    filter_button_done.click( function () {
        view_status = 1;
        focus_filter(view_status);
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i].status === true) {
                state_list[i].state = true;
            }
            else {
                state_list[i].state = false;
            }
        }
        current_page = 0;
        update();
        return false;
    });

    //Фильтрация невыполненных задач
    filter_button_not_done.click( function () {
        view_status = 2;
        focus_filter(view_status);
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i].status === false) {
                state_list[i].state = true;
            }
            else {
                state_list[i].state = false;
            }
        }
        current_page = 0;
        update();
        return false;
    });

    //Изменение состояния выполнения
    list__ul.on("click", ".li__button-change", function () {
        let state_mod = parseInt($(this).parent().children(".li__id").html());
        let state = state_list[state_mod].status;
        state_list[state_mod].status = !state;
        if (view_status != 0) {
            state_list[state_mod].state = false;
        }
        update();
        return false;
    });

    //Удаление элемента
    list__ul.on("click", ".list__ul_li .li__button-delete", function () {
        delete state_list[parseInt($(this).parent().children(".li__id").html())];
        clear_model();
        update();
        return false;
    });

    //Удалить все выполненные
    deleter_button.click( function () {
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i].status) {
                delete state_list[i];
            }
        }
        clear_model();
        update();
        return false;
    });

    //Отметить все задачи как выполненные
    changer_button_done.click( function () {
        for (i = 0; i < state_list.length; i++) {
            state_list[i].status = true;
            if (view_status === 2) { state_list[i].state = false; }
            else { state_list[i].state = true; }
        }
        update();
        return false;
    });

    //Отметить все задачи как невыполненные
    changer_button_not_done.click( function () {
        for (i = 0; i < state_list.length; i++) {
            state_list[i].status = false;
            if (view_status === 1) { state_list[i].state = false; }
            else { state_list[i].state = true; }
        }
        update();
        return false;
    });

    //Редактирование текста задачи и отслеживание нажатия Enter
    list__ul.on("keyup", ".list__ul_li .li__div-input input", function () {
        if (event.key === "Enter") {
            update();
        }
        else {
            let answe = false;
            let state_local = parseInt($(this).parent().parent().children(".li__id").html());
            state_list[state_local].data = _.escape(_.unescape($(this).val().trim()));
            if (answe) {
                state_list[state_local].data = ""
            }
        }
        return false;
    });

    //Двойное нажатие для редактирования
    list__ul.on("dblclick", ".list__ul_li .li__div-output", function () {
        let state = $(this).parent().children(".li__id");
        $(this).parent().children(".li__div-input").children("input").val(state_list[parseInt(state.html())].data);
        $(this).parent().children(".li__div-output").toggleClass("shadow-mask");
        $(this).parent().children(".li__div-input").toggleClass("shadow-mask");
        return false;
    });

    //Нажатие вне для окончания редактирования
    list__ul.on("blur", ".list__ul_li .li__div-input", function () {
        update();
        return false;
    });



    // Вспомогательные функции
    //Фокус фильтра
    function focus_filter(value) {
        filter_button_all.removeClass("filter__button_active");
        filter_button_done.removeClass("filter__button_active");
        filter_button_not_done.removeClass("filter__button_active");
        switch (value) {
            case 0:
                filter_button_all.addClass("filter__button_active");
                break;
            case 1:
                filter_button_done.addClass("filter__button_active");
                break;
            case 2:
                filter_button_not_done.addClass("filter__button_active");
                break;
        }
    }

    //Стартовая функция
    function start() {
        focus_filter(0);
        update();
    }

    //Проверка всех вводов на фокус
    function focus_control() {
        let result = true;
        for (i = 0; i < state_list.length; i++) {
            if ($(`.list__ul_li${i} .li__div-input input`).is(":focus")) {
                result = false;
            }
        }
        return result;
    }

    //Скрытие всех вводов
    function hide_inputs() {
        for (i = 0; i < state_list.length; i++) {
            $(`.list__ul_li${i} .li__div-input`).toggleClass("shadow-mask");
        }
    }


    //Очищение массива с данными от удалённых
    function clear_model() {
        let state = [];
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i] != undefined) {
                state.push(state_list[i]);
            }
        }
        state_list = state;
    }

    //Определение количества страниц
    function page_calculator() {
        let result = 0;
        let len = 0;
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i].state === true) {
                len += 1;
            }
        }
        result += (len - len % page_size) / page_size;
        if (len % page_size > 0) { result++; }
        return result;
    }

    //Определение значения счётчика
    function get_count_value () {
        let done = 0;
        let not_done = 0;
        let all = 0;
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i].status) { done ++; }
            else { not_done ++; }
            all ++;
        }
        return [done, not_done, all];
    }

    //Создание видимого списка
    function get_view_list() {
        let result = [];
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i].state === true) {
                result.push(i);
            }
        }
        return result;
    }

    //Очищение списка от пустых сохранённых значений
    function delete_empty() {
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i].data.trim() === '') {
                delete state_list[i];
            }
        }
        clear_model();
    }

    //Обновление и размещение содержимого
    function update() {
        clear();
        delete_empty();
        let page_num = page_calculator();
        let view_list = get_view_list();
        let state = get_count_value();

        if (current_page > page_num-1) { current_page = page_num - 1; }
        if (page_num === 0) { current_page = 0; }
        for (i = 0; i < page_size; i++) {
            if (current_page * page_size + i < view_list.length) {
                view_todo(view_list[current_page * page_size + i]);
            }
        }
        clear_page();
        view_page(page_num);
        focusing_page();
        view_counter(state[0], state[1], state[2]);
        view_text();
    }



    // Viewer
    //Визуализация задачи по id
    function view_todo(value) {
        list__ul.append(`
            <li class='fb fb_r-sb list__ul_li list__ul_li${value}'>
               <h5 class='li__id'>${value}</h5>
               <button class='li__button-change'></button>
               <div class='li__div-input'><input type='text' maxlength='144'></div>
               <div class='li__div-output'><h5>${state_list[value].data}</h5></div>
               <button class='li__button-delete'></button>
            </li>`);
        let text = state_list[value].status ? 'Выполнил' : 'Не выполнил';
        let addClass = state_list[value].status ? "li__button-change_done" : "li__button-change_not-done";
        let removeClass = state_list[value].status ? "li__button-change_not-done" : "li__button-change_done";
        list__ul_li_func(value).html(text);
        list__ul_li_func(value).addClass(addClass);
        list__ul_li_func(value).removeClass(removeClass);

        $(`.list__ul_li${value} .li__div-input`).toggleClass("shadow-mask");
    }

    //Визуализация пагенации
    function view_page(value) {
        let state = ``;
        for (i = 0; i < value; i++) {
            state += (`<button class='pager__button pager__button${i}'>${i+1}</button>`);
        }
        pager.append(state);
    }

    //Отображение даных из модели в тег текста задачи
    function view_text() {
        for (i = 0; i < state_list.length; i++) {
            $(`.list__ul_li${i}`).children(".li__div-output").children("h5").html(state_list[i].data);
        }
    }

    //Визуализация счётчиков
    function view_counter(value1, value2, value3) {
        counter_text_done.html("Выполнено задач : " + value1);
        counter_text_not_done.html("Не выполнено задач : " + value2);
        counter_text_all.html("Всего задач : " + value3);
    }

    //Отрисовываю фокусировку выбранной страницы
    function focusing_page() {
        $(`.pager__button${current_page}`).addClass("pager__button_active");
    }

    //Дополнительная очистка страниц
    function clear_page() {
        pager.empty();
    }

    //Очищение поле ввода текста новых задач
    function clear_input() {
        adder_input.val("");
    }

    //Очистка рабочего пространства
    function clear() {
        pager.empty();
        list__ul.empty();
        counter_text_done.empty();
        counter_text_not_done.empty();
        counter_text_all.empty();
    }

    //Сокрытие блока
    function in_shadow(value) {
        value.addClass("shadow-mask").removeClass("light-mask");
    }

    //Раскрыть блок
    function in_light(value) {
        value.removeClass("shadow-mask").addClass("light-mask");
    }
});
